#!/bin/sh
#SpringBoot应用启动脚本
#JAR构建包名称
JAR_NAME=blog.jar


rm -f tpid

#参数启动配置
nohup java -server -Xmx4g -Xms4g -Xss512k -XX:+AggressiveOpts -XX:+UseBiasedLocking \
-XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC \
-XX:+UseParNewGC -XX:+CMSParallelRemarkEnabled \
-XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods \
-XX:+UseCMSInitiatingOccupancyOnly \
-Djava.awt.headless=true \
-jar $JAR_NAME > /dev/null 2>&1 &

echo $! > tpid
echo "Start SpringBoot $JAR_NAME Done!"
