package cn.nxcoder.blog.service;


import cn.nxcoder.blog.core.Service;
import cn.nxcoder.blog.entity.Tag;
import cn.nxcoder.blog.entity.Type;

import java.util.List;


public interface TagService extends Service<Tag> {
    List<Tag> findTop10Tags();

    List<Tag> findAllTags();
}
