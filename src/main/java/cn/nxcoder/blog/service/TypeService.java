package cn.nxcoder.blog.service;


import cn.nxcoder.blog.core.Service;
import cn.nxcoder.blog.entity.Type;

import java.util.List;


public interface TypeService extends Service<Type> {

    List<Type> findTop6Types();

    List<Type> findAllTypes();

}
