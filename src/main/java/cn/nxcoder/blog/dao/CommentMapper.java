package cn.nxcoder.blog.dao;

import cn.nxcoder.blog.core.MyMapper;
import cn.nxcoder.blog.entity.Comment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 博客Mapper
 * @author shengwu ni
 * @date 2018/05/09 22:37
 */
public interface CommentMapper extends MyMapper<Comment> {

    @Select("select * from nx_comment where blogid =#{blogId} and parentid = #{parentId} order by create_time asc")
    @ResultMap("BaseResultMap")
    List<Comment> findByBlogIdAndParentId(@Param("blogId") Long blogId, @Param("parentId") Long parentId);

    @Select("select * from nx_comment where id = #{id}")
    Comment findById(Long id);
}
