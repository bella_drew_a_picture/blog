package cn.nxcoder.blog.dao;

import cn.nxcoder.blog.core.MyMapper;
import cn.nxcoder.blog.entity.Tag;
import org.apache.ibatis.annotations.Delete;

/**
 * 博客标签Mapper
 * @author shengwu ni
 * @date 2018/05/05 15:32
 */
public interface TagMapper extends MyMapper<Tag> {

    /**
     * 根据id删除博客标签
     * @param id id
     * @return Integer
     */
    @Delete("delete from nx_tag where id = #{id}")
    Integer deleteById(Long id);
}
