package cn.nxcoder.blog.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * 博客标签实体
 * @author shengwu ni
 * @date 2018/05/04 21:34
 */
@Table(name = "nx_tag")
public class Tag {

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 标签名称
     */
    @NotBlank(message = "标签内容不能为空")
    private String name;

    /**
     * 该标签下的博客id
     */
    private String blogid;

    /**
     * 该标签下的博客数量，用于前端显示
     */
    @Transient
    private Integer blogNum;

    public Tag() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlogid() {
        return blogid;
    }

    public void setBlogid(String blogid) {
        this.blogid = blogid;
    }

    public Integer getBlogNum() {
        return blogNum;
    }

    public void setBlogNum(Integer blogNum) {
        this.blogNum = blogNum;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
