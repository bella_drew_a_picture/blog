package cn.nxcoder.blog.controller.admin;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 关于我
 * @author shengwu ni
 * @date 2018/05/12 22:31
 */
@Controller
public class AboutController {

    @GetMapping("/about")
    public String about(Model model) {
        model.addAttribute("title", "关于博主");
        return "about";
    }
}
