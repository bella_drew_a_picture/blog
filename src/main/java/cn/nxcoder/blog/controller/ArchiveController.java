package cn.nxcoder.blog.controller;

import cn.nxcoder.blog.entity.Blog;
import cn.nxcoder.blog.entity.Type;
import cn.nxcoder.blog.entity.User;
import cn.nxcoder.blog.model.BlogVo;
import cn.nxcoder.blog.model.Page;
import cn.nxcoder.blog.service.BlogService;
import cn.nxcoder.blog.service.TypeService;
import cn.nxcoder.blog.service.UserService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 博客归档页面
 * @author shengwu ni
 * @date 2018/05/12 16:45
 */
@Controller
public class ArchiveController {

    @Resource
    private BlogService blogService;

    @GetMapping("/archives")
    public String archives(Model model) {

        Map<String, List<Blog>> archives = blogService.archiveBlogList();
        model.addAttribute("archiveMap", archives);
        model.addAttribute("blogCount", blogService.blogCount());
        model.addAttribute("title", "博客归档");
        return "archives";
    }

}
