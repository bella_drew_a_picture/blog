package cn.nxcoder.blog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 自定义博客不存在异常, @ResponseStatus(HttpStatus.NOT_FOUND)注解用来指定该异常去404页面
 * @author shengwu ni
 * @date 2018/05/04 20:07
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NonBlogException extends RuntimeException {

    public NonBlogException() {
    }

    public NonBlogException(String message) {
        super(message);
    }

    public NonBlogException(String message, Throwable cause) {
        super(message, cause);
    }
}
