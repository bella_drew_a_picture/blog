package cn.nxcoder.blog.core;

import org.apache.ibatis.exceptions.TooManyResultsException;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * Service 层 基础接口，其他Service 接口 请继承该接口
 * @author shengwu ni
 * @date  2018/05/05 01:17
 * @param <T> T
 */
public interface Service<T> {
    /**
     * 单个保存
     * @param model model
     */
    Integer save(T model);

    /**
     * 批量保存
     * @param models models
     */
    Integer save(List<T> models);

    /**
     * 通过主鍵刪除
     * @param id id
     */
    Integer deleteById(Long id);

    /**
     * 批量刪除 eg：ids -> “1,2,3,4”
     * @param ids ids
     */
    Integer deleteByIds(String ids);

    /**
     * 更新
     * @param model model
     */
    Integer update(T model);

    /**
     * 通过ID查找
     * @param id id
     * @return T
     */
    T findById(Long id);

    /**
     * 通过Model中某个成员变量名称（非数据表中column的名称）查找,value需符合unique约束
     * @param fieldName fieldName
     * @param value value
     * @return T
     * @throws TooManyResultsException TooManyResultsException
     */
    T findBy(String fieldName, Object value) throws TooManyResultsException;

    /**
     * 通过多个ID查找//eg：ids -> “1,2,3,4”
     * @param ids ids
     * @return List<T>
     */
    List<T> findByIds(String ids);

    /**
     * 根据条件查找
     * @param condition condition
     * @return List<T>
     */
    List<T> findByCondition(Condition condition);

    /**
     * 获取所有
     * @return List<T>
     */
    List<T> findAll();

    /**
     * 根据条件获取
     * @param example example
     * @return List<T>
     */
    List<T> selectByExample(Example example);
}
